﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disaheim
{
        public class Utility
        {
            /*Brug Utility-klassen i dit UtilityLib-bibliotek fra forrige opgave til at beregne den totale værdi for de enkelte produkter.
            Fordi attributterne Book, amulet og Course nu er blevet private skal de implementeres som en field. 
            Bemærk: Hvis et privat C# felt fungerer som et ’backing field’ for en property, da skal feltnavn foranstilles med en underscore (_).
            Jeg skal lave nye funktioner for amulet og book men jeg beholder timeprisen for Course. 
            Derfor udkommenderes de gamle funktioner for book og amulet, og de sættes sammen som en samlet Get Value for Merchandise.
            Inden dette skal jeg lave funktioner for beregningen af amulettens Quality level, og tilsidst lægges prisen for Quality level 
            sammen med prisen for book. Tilsammen bliver det Get value of merchandise.
            */

            // atrributten amulet er privat og skal implementeres som field med foranstillet underscore.Bruger double til beregning.
            private double _lowQualityValue = 12.5;
            private double _mediumQualityValue = 20.0;
            private double _highQualityValue = 27.5;
            //timeprisen for kurset er 875. Den skal tilegnes et navn fx Hourprice. Dette navn skal jeg tilrette nederst i min beregning for GetValueOfCourse
            private double _courseHourValue = 875;

            public double LowQualityValue 
            {
                get { return _lowQualityValue; }
                set { _lowQualityValue = value;}
            }
            public double MediumQualityValue
            {
                get { return _mediumQualityValue;}
                set { _mediumQualityValue = value;}
            }
            public double ValueAmuletQualityhigh
            {
                get { return _highQualityValue;}
                set { _highQualityValue = value;}
            }
            public double CourseHourValue
            {
                get { return _courseHourValue;}
                set { _courseHourValue = value;}
            }

            /* Nu har jeg lavet beregningen af amulettens Quality level, og nu lægges prisen for Quality level 
            sammen med prisen for book.Tilsammen bliver det Get value of merchandise.*/
            public double GetValueOfMerchandise(Merchandise merchandise) //dvs book prisen plus prisen for amulet
            {
            if (merchandise is Book book)
            {
                return book.Price;
            }
            else if (merchandise is Amulet amulet)
            {
                if (amulet.Quality == Level.low)
                {
                    return LowQualityValue;
                }
                if (amulet.Quality == Level.medium)
                {
                    return MediumQualityValue;
                }
                if (amulet.Quality == Level.high)
                {
                    return ValueAmuletQualityhigh;
                }
                return 0;
            }
            return 0;
            }
            public double GetValueOfAmulet(Amulet amulet)
            {
            if (amulet.Quality == Level.low)
            {
                return _lowQualityValue;
            }
            if (amulet.Quality == Level.medium)
            {
                return (_mediumQualityValue);
            }
            if (amulet.Quality == Level.high)
            {
                return _highQualityValue;
            }
            return 0;
            }

            //875 kr i timen
            public double GetValueOfCourse(Course course)
            {
                if (course.DurationInMinutes == 0)
                {
                    return 0;
                }

                // Hvis der ikke er en rest kan man bare gange hver time.
                if (course.DurationInMinutes % 60 == 0)
                {
                    double courseValue = (course.DurationInMinutes / 60) * CourseHourValue;
                    return courseValue;
                }

                // Når der ikke er rest skal man bare tilføje 1 hel til hver påbegyndt time
                int courseHours = (course.DurationInMinutes / 60) + 1;
                double coursePrice = courseHours * CourseHourValue;
                return coursePrice;
            } 
            /*udkommanderes pga ny funktion Getvalue of Merchandise
            public double GetValueOfBook(Book book)
            {
                return book.Price;
            }
            */

            /* udkommanderes pga ny funktion Getvalue of Merchandise
            public double GetValueOfAmulet(Amulet amulet)
            {
            if (amulet.Quality == Level.low)
            {
                return 12.5;
            }
            if (amulet.Quality == Level.medium)
            {
                return 20.0;
            }
            if (amulet.Quality == Level.high)
            {
                return 27.5;
            }
            return 0;
            */
        }
    }





﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disaheim
{
    public class Book : Merchandise //Henter fra klassen Merchandise
    {
        
        /* Dette fjernes
        private string itemid;
        public string ItemID 
        { 
            get { return itemid; }
            set { itemid = value;}
        }
        */
        private string title;
        public string Title 
        { 
            get { return title;}
            set { title = value;}  
        }
        public double Price { get; set; }

        public Book(string itemid)
        {
            base.ItemId = itemid; // Calling the base class method. Og ændre navnet til ItemID
        }
        public Book(string itemid, string title)
        {
            base.ItemId = itemid; // Calling the base class method. Og ændre navnet til ItemID
            this.title = title;
        }
        public Book(string itemid, string title, double Price) //i opaven står den præcise rækkefølge som er Itemid, quality og design til sidst. 
                                                               //derfor skal man sætte den i samme rækkefølge
        {
            base.ItemId = itemid; // Calling the base class method. Og ændre navnet til ItemID
            this.title = title;
            this.Price = Price;
        }

        //Implementér ToString()-metoden, som er en override.
        public override string ToString() //ToString() : string: returnerer en tekst-streng med ItemID, Title og Price
        {
            // "ItemId: 1, Title: , Price: 0" Linie 45 skal sættes præcist som der står i unittesten som er kopieret ind her. 
            return $"ItemId: {ItemId}, Title: {Title}, Price: {Price}";




        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Security;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Disaheim
{
    public class Amulet : Merchandise    //Henter fra klassen Merchandise
    {
        /* Dette fjernes
        private string itemid;
        public string ItemID 
        { 
            get { return itemid; }
            set { itemid = value;}
        }
        */

        private string design;

        public string Design
        {
            get { return design; }
            set { design = value; }
        }

        public Level Quality { get; set; } //Quality er en Level, det står i opgavebeskrivelsen, derfor skal den have enum fordi quality er enten low, medium eller high

        public Amulet(string itemid)
        {
            base.ItemId = itemid;    // Calling the base class method. Og ændre navnet til ItemID
        }
        public Amulet(string itemid, Level Quality)
        {
            base.ItemId = itemid;   // Calling the base class method. Og ændre navnet til ItemID
            this.Quality = Quality;
        }
        public Amulet(string itemid, Level Quality, string design)  //i opaven står den præcise rækkefølge som er Itemid, quality og design til sidst. 
                                                                    //derfor skal man sætte den i samme rækkefølge
        {
            base.ItemId = itemid;       // Calling the base class method. Og ændre navnet til ItemID
            this.Quality = Quality;
            this.design = design;
        }
        //Implementér ToString()-metoden, som er en override.
        public override string ToString() //ToString() : string: returnerer en tekst-streng med ItemID, Design og Quality
        {
            //"ItemId: 11, Quality: medium, Design: " Linie 50 skal sættes præcist som der står i unittesten som er koiperet ind her. 
            //Tutor ved ikke hvorfor den giver fejl/
            return $"ItemId: {ItemId}, Quality: {Quality}, Design: {Design}";

        }
    }
} 


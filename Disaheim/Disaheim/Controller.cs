﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disaheim
{
    public class Controller
    {
        //anvend List<T>-datatypen
        public List<Book> Books { get; set; }   
        public List<Amulet> Amulets { get; set; }

        //tilføj Courses
        public List<Course> Courses { get; set; }

        //De to properties Books og Amulets skal initialiseres i constructor’en for Controller-klassen
        public Controller() 
        {
            Books = new List<Book>();
            Amulets = new List<Amulet>();
            Courses = new List<Course>(); //Her er course også tilføjet
        }
        //Implementér de to operationer AddToList() i designklassen som C# metode. 
        //der er faktisk kun én operation, men med to forskellige typer af parametre. Dette kan løses med metode-overloading
        public void AddToList(Book book) 
        { 
            Books.Add(book); 
        }
        public void AddToList(Amulet amulet)
        {
            Amulets.Add(amulet);
        }
        public void AddToList(Course course) //her er course også tilføjet
        {
            Courses.Add(course);
        }
    }
    
}

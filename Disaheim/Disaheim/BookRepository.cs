﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disaheim
{
    public class BookRepository
    {
        private List<Book> books = new List<Book>(); //gør private og anvend List<T>-datatypen

        //public og add book
        public void AddBook(Book book)
        {
            books.Add(book);
        }
        //public og add get book.
        public Book GetBook(string itemId)
        {
            Book book = GetBook(itemId);
            return book;
        }
        //public og ad book price. Dvs skal tage udgangspunkt i Utility class som samler den totale pris
        public double GetTotalValue()
        {
            Utility utility = new Utility();
            double total = 0;
            foreach (Book book in books)
            {
                return book.Price;
            }
            return total;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Disaheim
{
    public class CourseRepository
    {
        private List<Course> courses = new List<Course>();  //gør private og anvend List<T>-datatypen

        //public og add course
        public void AddCourse (Course course)
        {
            courses.Add(course);
        }
        //public og add get course. Husk brug Name som parameter til GetCourse i stedet for ItemId
        public Course GetCourse(string name) 
        {
            Course course = GetCourse(name);
            return course;
        }
        //public og ad get total value. Dvs skal tage udgangspunkt i Utility class som samler den totale pris
        public double GetTotalValue()
        {
            Utility utility = new Utility();
            double total = 0;
            foreach (Course course in courses)
            {
                total += utility.GetValueOfCourse(course);
            }
            return total;
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disaheim
{
    public class AmuletRepository
    {
        private List<Amulet> amulets = new List<Amulet>(); //gør private og anvend List<T>-datatypen

        //public og add amulet
        public void AddAmulet(Amulet amulet)
        {
            amulets.Add(amulet);
        }
        //public og add get amulet.
        public Amulet GetAmulet(string itemId)
        {
            Amulet amulet = GetAmulet(itemId);
            return amulet;
        }
        //public og ad get total value. Dvs skal tage udgangspunkt i Utility class som samler den totale pris
        public double GetTotalValue()
        {
                Utility utility = new Utility();
                double total = 0;
                foreach (Amulet amulet in amulets)
                {
                    total += utility.GetValueOfAmulet(amulet);
                }
                return total;

                /*if (amulet is Amulet amulet)
                {
                    if (amulet.Quality == Level.low)
                    {
                        return ValueAmuletQualitylow;
                    }
                    if (amulet.Quality == Level.medium)
                    {
                        return ValueAmuletQualitymedium;
                    }
                    if (amulet.Quality == Level.high)
                    {
                        return ValueAmuletQualityhigh;
                    }
                    return 0;
                }
                return 0;
                */
            }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Disaheim
{

    public class MerchandiseRepository
    // Book og amulet er i DCD sat som Merhcandise derfor hedder dette repository Merchandise som fælles betegnelse
    //Benyt igen List<T> til at repræsentere de private felter
    {
        private List<Merchandise> merchandises = new List<Merchandise>();  //gør private og anvend List<T>-datatypen

        //public og add amulet og book
        public void AddMerchandise(Merchandise merchandise)
        {
            merchandises.Add(merchandise);
        }

        //public og add get book og get amulet
        public Merchandise GetMerchandise(string ItemId)
        {
            Merchandise merchandise = GetMerchandise(ItemId);
            return merchandise;
        }
         
        //public og add get total value.
        public double GetTotalValue()
        {
            Utility utility = new Utility();
            double total = 0;
            foreach (Merchandise merchandise in merchandises)
            {
                total += utility.GetValueOfMerchandise(merchandise);
            }
            return total;
        }
    }        
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disaheim
{
    public abstract class Merchandise
    {
        public string ItemId { get; set; } // Properties


        /* Skal fjernes
        public Merchandise(string itemId) 
        { 
            this.ItemId = itemId;
        }
        */

        public override string ToString() // Methode
        {
            return $"ItemId: {ItemId}";
        }
    }
}
